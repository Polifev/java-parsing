package hepl.genielogiciel.model.wmc;

import static org.junit.Assert.assertEquals;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.Test;

import hepl.genielogiciel.Java8Lexer;
import hepl.genielogiciel.Java8Parser;
import hepl.genielogiciel.Java8Visitor;

public class MethodsVisitorTest {

	@Test
	public void zeroIfNoMethods() {
		String code = "public class MyClass{\r\n" + "\r\n" + "}";
		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(code));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();
		Java8Visitor<Integer> visitor = new MethodsVisitor();
		int result = visitor.visit(tree);
		assertEquals(0, result);
	}

	@Test
	public void countMethods() {
		String code = "public class MyClass{\r\n" + "	public void method1(){\r\n" + "	\r\n" + "	}\r\n" + "	\r\n"
				+ "	public void method2(){\r\n" + "	\r\n" + "	}\r\n" + "}\r\n";
		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(code));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();
		Java8Visitor<Integer> visitor = new MethodsVisitor();
		int result = visitor.visit(tree);
		assertEquals(2, result);
	}

	@Test
	public void sumMethodsComplexities() {
		String code = "public class MyClass{\r\n" + "	public void method1(){\r\n" + "		if(a){\r\n" + "		\r\n"
				+ "		}\r\n" + "	}\r\n" + "	\r\n" + "	public void method2(){\r\n" + "		if(a){\r\n"
				+ "		\r\n" + "		}\r\n" + "		if(b){\r\n" + "		\r\n" + "		}\r\n" + "	}\r\n"
				+ "}\r\n";
		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(code));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();
		Java8Visitor<Integer> visitor = new MethodsVisitor();
		int result = visitor.visit(tree);
		assertEquals(6, result);
	}

	@Test
	public void multiplySuccessiveBranches() {
		String code = "public class MyClass{\r\n" + "	public void method2(){\r\n" + "		if(a){\r\n" + "		\r\n"
				+ "		}\r\n" + "		if(b){\r\n" + "		\r\n" + "		}\r\n" + "	}\r\n" + "}\r\n" + "";
		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(code));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();
		Java8Visitor<Integer> visitor = new MethodsVisitor();
		int result = visitor.visit(tree);
		assertEquals(4, result);
	}

	@Test
	public void sumNestedBranches() {
		String code = "public class MyClass{\r\n" + "		public void method2(){\r\n" + "			if(a){\r\n"
				+ "				if(b){\r\n" + "			\r\n" + "				}\r\n" + "			}\r\n"
				+ "		}\r\n" + "	}";
		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(code));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();
		Java8Visitor<Integer> visitor = new MethodsVisitor();
		int result = visitor.visit(tree);
		assertEquals(3, result);
	}

	@Test
	public void sumSwitchCases() {
		String code = "public class MyClass{\r\n" + "	public void method2(){\r\n" + "		switch(a) {\r\n"
				+ "			case 'a':\r\n" + "				break;\r\n" + "			case'b':\r\n"
				+ "				break;\r\n" + "			case'c':\r\n" + "				break;\r\n"
				+ "			default:\r\n" + "				break;\r\n" + "		}\r\n" + "	}\r\n" + "}";
		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(code));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();
		Java8Visitor<Integer> visitor = new MethodsVisitor();
		int result = visitor.visit(tree);
		assertEquals(4, result);
	}

	@Test
	public void handleEmbeddedInSwitchCases() {
		String code = "public class MyClass{\r\n" + "	public void method2(){\r\n" + "		switch(a) {\r\n"
				+ "			case 'a':\r\n" + "				if(a){\r\n" + "				\r\n"
				+ "				} else{\r\n" + "				\r\n" + "				}\r\n"
				+ "				break;\r\n" + "			case'b':\r\n" + "				break;\r\n"
				+ "			case'c':\r\n" + "				break;\r\n" + "			default:\r\n"
				+ "				break;\r\n" + "		}\r\n" + "	}\r\n" + "}";
		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(code));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();
		Java8Visitor<Integer> visitor = new MethodsVisitor();
		int result = visitor.visit(tree);
		assertEquals(5, result);
	}

	@Test
	public void multiplyMultipleSwitches() {
		String code = "public class MyClass{\r\n" + "	public void method2(){\r\n" + "		switch(a) {\r\n"
				+ "			case 'a':\r\n" + "				break;\r\n" + "			case'b':\r\n"
				+ "				break;\r\n" + "			case'c':\r\n" + "				break;\r\n"
				+ "			default:\r\n" + "				break;\r\n" + "		}\r\n" + "		switch(a) {\r\n"
				+ "			case 'a':\r\n" + "				break;\r\n" + "			case'b':\r\n"
				+ "				break;\r\n" + "			default:\r\n" + "				break;\r\n" + "		}\r\n"
				+ "	}\r\n" + "}";
		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(code));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();
		Java8Visitor<Integer> visitor = new MethodsVisitor();
		int result = visitor.visit(tree);
		assertEquals(12, result);
	}

	@Test
	public void countAllLoops() {
		String code = "public class MyClass{\r\n" + "	public void method(){\r\n" + "		while(a){}\r\n"
				+ "		for(int i = 0; i < b; i++) {}\r\n" + "		for(Obj o : col){}\r\n" + "		do{}while(c);\r\n"
				+ "	}\r\n" + "}\r\n" + "";
		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(code));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();
		Java8Visitor<Integer> visitor = new MethodsVisitor();
		int result = visitor.visit(tree);
		assertEquals(16, result);
	}
}
