package hepl.genielogiciel.model;

import static org.junit.Assert.assertEquals;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

public class DeclarationMapTest {

	@Test
	public void canGetClassNameForGloballyDeclaredVariables() {
		String className = "MyClass";
		String varName = "obj";
		DeclarationMap map = new DeclarationMap();
		map.addVariable(DeclarationMap.GLOBAL_CONTEXT, new VariableDeclaration(className, varName));
		assertEquals(className, map.getClassName(DeclarationMap.GLOBAL_CONTEXT, varName));
	}

	@Test
	public void canGetClassNameForLocallyDeclaredVariables() {
		String className = "MyClass";
		String varName = "obj";
		String method = "doSomething()";

		DeclarationMap map = new DeclarationMap();
		map.addVariable(method, new VariableDeclaration(className, varName));
		assertEquals(className, map.getClassName(method, varName));
	}

	@Test
	public void contextSelectionWithParametrizedMethodsWorks() {
		String className = "MyClass";
		String varName = "obj";
		String method = "doSomething(int a)";

		DeclarationMap map = new DeclarationMap();
		map.addVariable(method, new VariableDeclaration(className, varName));
		assertEquals(className, map.getClassName(method, varName));
	}

	@Test
	public void searchGlobalContextIfNotFoundInLocalContext() {
		String className = "MyClass";
		String varName = "obj";
		String method = "doSomething(int a)";

		DeclarationMap map = new DeclarationMap();
		map.addVariable(DeclarationMap.GLOBAL_CONTEXT, new VariableDeclaration(className, varName));
		assertEquals(className, map.getClassName(method, varName));
	}

	@Test
	public void prioritizeLocalVariablesWhenFoundInLocalContext() {
		String globalClassName = "MyFirstClass";
		String localClassName = "MySecondClass";
		String varName = "obj";
		String method = "doSomething()";

		DeclarationMap map = new DeclarationMap();
		map.addVariable(DeclarationMap.GLOBAL_CONTEXT, new VariableDeclaration(globalClassName, varName));
		map.addVariable(method, new VariableDeclaration(localClassName, varName));

		assertEquals(localClassName, map.getClassName(method, varName));
	}

	@Test
	public void addMultipleVariablesToAContext() {
		String context = DeclarationMap.GLOBAL_CONTEXT;
		String class1 = "MyFirstClass";
		String class2 = "MySecondClass";
		String var1 = "var1";
		String var2 = "var2";

		List<VariableDeclaration> declarations = new LinkedList<VariableDeclaration>();
		declarations.add(new VariableDeclaration(class1, var1));
		declarations.add(new VariableDeclaration(class2, var2));

		DeclarationMap map = new DeclarationMap();
		map.addVariables(context, declarations);

		assertEquals(class1, map.getClassName(context, var1));
		assertEquals(class2, map.getClassName(context, var2));
	}
}
