package hepl.genielogiciel.model.atfd;

import static org.junit.Assert.assertEquals;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.Test;

import hepl.genielogiciel.Java8Lexer;
import hepl.genielogiciel.Java8Parser;
import hepl.genielogiciel.model.AccessMap;
import hepl.genielogiciel.model.DeclarationMap;
import hepl.genielogiciel.model.VariableDeclaration;

public class ForeignAccessVisitorTest {
	@Test
	public void countSetAttributesInMethod() {
		String testClass = "public class GreatClass {\r\n" + "	\r\n" + "	  public void doSomething() {\r\n"
				+ " 	  MyOtherClass obj1 = new MyOtherClass();\r\n" + " 	  obj1.prop1 = 12;\r\n" + "	  }\r\n"
				+ "	}";

		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(testClass));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();

		DeclarationMap declarationsMap = new DeclarationMap();
		declarationsMap.addVariable("doSomething()", new VariableDeclaration("MyOtherClass", "obj1"));

		ForeignAccessVisitor visitor = new ForeignAccessVisitor(declarationsMap);
		AccessMap accessMap = visitor.visit(tree);
		int count = accessMap.computeResultingCount();

		assertEquals(1, count);
	}

	@Test
	public void countSetAttributeInConstructor() {
		String testClass = "public class GreatClass {\r\n" + "	\r\n" + "	  public GreatClass() {\r\n"
				+ " 	  MyOtherClass obj1 = new MyOtherClass();\r\n" + " 	  obj1.prop1 = 12;\r\n" + "	  }\r\n"
				+ "	}";

		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(testClass));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();

		DeclarationMap declarationsMap = new DeclarationMap();
		declarationsMap.addVariable("GreatClass()", new VariableDeclaration("MyOtherClass", "obj1"));

		ForeignAccessVisitor visitor = new ForeignAccessVisitor(declarationsMap);
		AccessMap accessMap = visitor.visit(tree);
		int count = accessMap.computeResultingCount();

		assertEquals(1, count);
	}

	@Test
	public void countGetAttributeInConstructor() {
		String testClass = "public class GreatClass {\r\n" + "	\r\n" + "	  public GreatClass() {\r\n"
				+ " 	  MyOtherClass obj1 = new MyOtherClass();\r\n" + " 	  int a = obj1.prop1;\r\n" + "	  }\r\n"
				+ "	}";

		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(testClass));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();

		DeclarationMap declarationsMap = new DeclarationMap();
		declarationsMap.addVariable("GreatClass()", new VariableDeclaration("MyOtherClass", "obj1"));

		ForeignAccessVisitor visitor = new ForeignAccessVisitor(declarationsMap);
		AccessMap accessMap = visitor.visit(tree);
		int count = accessMap.computeResultingCount();

		assertEquals(1, count);
	}

	@Test
	public void countGetAttributesInMethod() {
		String testClass = "public class GreatClass {\r\n" + "	\r\n" + "	  public void doSomething() {\r\n"
				+ " 	  MyOtherClass obj1 = new MyOtherClass();\r\n"
				+ " 	  AnotherClass obj2 = new AnotherClass();\r\n" + " 	  int a = obj1.prop1;\r\n"
				+ " 	  int b = obj2.prop1;\r\n" + "	  }\r\n" + "	}";

		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(testClass));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();

		DeclarationMap declarationsMap = new DeclarationMap();
		declarationsMap.addVariable("doSomething()", new VariableDeclaration("MyOtherClass", "obj1"));
		declarationsMap.addVariable("doSomething()", new VariableDeclaration("AnotherClass", "obj2"));

		ForeignAccessVisitor visitor = new ForeignAccessVisitor(declarationsMap);
		AccessMap accessMap = visitor.visit(tree);
		int count = accessMap.computeResultingCount();

		assertEquals(2, count);
	}

	@Test
	public void countGettersSettersCallInConstructor() {
		String testClass = "public class GreatClass {\r\n" + "	\r\n" + "	  public void GreatClass() {\r\n"
				+ " 	  MyOtherClass obj1 = new MyOtherClass();\r\n"
				+ " 	  AnotherClass obj2 = new AnotherClass();\r\n"
				+ " 	  EncoreAnotherClass obj3 = new EncoreAnotherClass();\r\n" + " 	  obj1.doSomething();\r\n"
				+ " 	  int b = obj2.getProp();\r\n" + " 	  obj3.setProp(b);\r\n" + "	  }\r\n" + "	}";

		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(testClass));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();

		DeclarationMap declarationsMap = new DeclarationMap();
		declarationsMap.addVariable("GreatClass()", new VariableDeclaration("MyOtherClass", "obj1"));
		declarationsMap.addVariable("GreatClass()", new VariableDeclaration("AnotherClass", "obj2"));
		declarationsMap.addVariable("GreatClass()", new VariableDeclaration("EncoreAnotherClass", "obj3"));

		ForeignAccessVisitor visitor = new ForeignAccessVisitor(declarationsMap);
		AccessMap accessMap = visitor.visit(tree);
		int count = accessMap.computeResultingCount();

		assertEquals(2, count);
	}

	@Test
	public void countGettersSettersCallInMethod() {
		String testClass = "public class GreatClass {\r\n" + "	\r\n" + "	  public void doSomething() {\r\n"
				+ " 	  MyOtherClass obj1 = new MyOtherClass();\r\n"
				+ " 	  AnotherClass obj2 = new AnotherClass();\r\n"
				+ " 	  EncoreAnotherClass obj3 = new EncoreAnotherClass();\r\n" + " 	  obj1.doSomething();\r\n"
				+ " 	  int b = obj2.getProp();\r\n" + " 	  obj3.setProp(b);\r\n" + "	  }\r\n" + "	}";

		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(testClass));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();

		DeclarationMap declarationsMap = new DeclarationMap();
		declarationsMap.addVariable("doSomething()", new VariableDeclaration("MyOtherClass", "obj1"));
		declarationsMap.addVariable("doSomething()", new VariableDeclaration("AnotherClass", "obj2"));
		declarationsMap.addVariable("doSomething()", new VariableDeclaration("EncoreAnotherClass", "obj3"));

		ForeignAccessVisitor visitor = new ForeignAccessVisitor(declarationsMap);
		AccessMap accessMap = visitor.visit(tree);
		int count = accessMap.computeResultingCount();

		assertEquals(2, count);
	}

	@Test
	public void ignoreAccessToItsOwnClass() {
		String testClass = "public class GreatClass {\r\n" + "	\r\n" + "	  public void doSomething() {\r\n"
				+ " 	  GreatClass obj1 = new GreatClass();\r\n" + " 	  AnotherClass obj2 = new AnotherClass();\r\n"
				+ " 	  EncoreAnotherClass obj3 = new EncoreAnotherClass();\r\n" + " 	  int b = obj1.getProp();\r\n"
				+ " 	  obj3.setProp(b);\r\n" + "	  }\r\n" + "	}";

		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(testClass));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();

		DeclarationMap declarationsMap = new DeclarationMap();
		declarationsMap.addVariable("doSomething()", new VariableDeclaration("GreatClass", "obj1"));
		declarationsMap.addVariable("doSomething()", new VariableDeclaration("AnotherClass", "obj2"));
		declarationsMap.addVariable("doSomething()", new VariableDeclaration("EncoreAnotherClass", "obj3"));

		ForeignAccessVisitor visitor = new ForeignAccessVisitor(declarationsMap);
		AccessMap accessMap = visitor.visit(tree);
		int count = accessMap.computeResultingCount();

		assertEquals(1, count);
	}
}
