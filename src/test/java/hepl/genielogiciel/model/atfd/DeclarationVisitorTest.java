package hepl.genielogiciel.model.atfd;

import static org.junit.Assert.assertEquals;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.Test;

import hepl.genielogiciel.Java8Lexer;
import hepl.genielogiciel.Java8Parser;
import hepl.genielogiciel.model.DeclarationMap;

public class DeclarationVisitorTest {
	@Test
	public void gatherFieldsDeclarations() {
		String testClass = "public class GreatClass {\r\n" + "	private MyOtherClass obj1;\r\n"
				+ "	private MyOtherClass obj2;\r\n" + "	public int a;\r\n" + "}";

		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(testClass));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();
		DeclarationVisitor visitor = new DeclarationVisitor();
		DeclarationMap map = visitor.visit(tree);

		assertEquals("MyOtherClass", map.getClassName(DeclarationMap.GLOBAL_CONTEXT, "obj1"));
		assertEquals("MyOtherClass", map.getClassName(DeclarationMap.GLOBAL_CONTEXT, "obj2"));
		assertEquals("int", map.getClassName(DeclarationMap.GLOBAL_CONTEXT, "a"));
	}

	@Test
	public void gatherLocalDeclarationsInConstructor() {
		String testClass = "public class GreatClass {\r\n" + "	\r\n" + "	  public GreatClass() {\r\n"
				+ " 	  MyOtherClass obj1 = null;\r\n" + "	  }\r\n" + "	}";

		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(testClass));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();
		DeclarationVisitor visitor = new DeclarationVisitor();
		DeclarationMap map = visitor.visit(tree);

		assertEquals("MyOtherClass", map.getClassName("GreatClass()", "obj1"));
	}

	@Test
	public void gatherLocalDeclarationsInMethod() {
		String testClass = "public class GreatClass {\r\n" + "	\r\n" + "	  public void doSomething() {\r\n"
				+ " 	  MyOtherClass obj1 = null;\r\n" + "	  }\r\n" + "	}";

		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(testClass));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();
		DeclarationVisitor visitor = new DeclarationVisitor();
		DeclarationMap map = visitor.visit(tree);

		assertEquals("MyOtherClass", map.getClassName("doSomething()", "obj1"));
	}

	@Test
	public void gatherParameterDeclarationsInConstructor() {
		String testClass = "public class GreatClass {\r\n" + "	\r\n"
				+ "	  public GreatClass(MyOtherClass param1) {\r\n" + "	  }\r\n" + "	}";

		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(testClass));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();
		DeclarationVisitor visitor = new DeclarationVisitor();
		DeclarationMap map = visitor.visit(tree);

		assertEquals("MyOtherClass", map.getClassName("GreatClass(MyOtherClass param1)", "param1"));
	}

	@Test
	public void gatherParameterDeclarationsInMethod() {
		String testClass = "public class GreatClass {\r\n" + "	\r\n"
				+ "	  public void doSomething(MyOtherClass param1, int a) {\r\n" + "	  }\r\n" + "	}";

		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(testClass));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();
		DeclarationVisitor visitor = new DeclarationVisitor();
		DeclarationMap map = visitor.visit(tree);

		assertEquals("MyOtherClass", map.getClassName("doSomething(MyOtherClass param1, int a)", "param1"));
		assertEquals("int", map.getClassName("doSomething(MyOtherClass param1, int a)", "a"));
	}
}
