package hepl.genielogiciel.model.tcc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.Test;

import hepl.genielogiciel.Java8Lexer;
import hepl.genielogiciel.Java8Parser;

public class PublicMethodsConnectionsMapVisitorTest {

	@Test
	public void passAttributeAsParameter() {
		String code = "public class Help{\r\n" + "    private int a;\r\n" + "    \r\n"
				+ "    public void doSomething(){\r\n" + "        print(a);\r\n" + "    }\r\n" + "}";
		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(code));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();
		Set<String> classFields = new HashSet<String>();
		classFields.add("a");
		PublicMethodsConnectionsMapVisitor visitor = new PublicMethodsConnectionsMapVisitor(classFields);
		ConnectionsMap map = visitor.visit(tree);
		assertTrue(map.getMap().get("doSomething()").contains("a"));
		assertEquals(map.getMap().get("doSomething()").size(), 1);
	}

	@Test
	public void invoqueMethodeOnClassField() {
		String code = "public class Help{\r\n" + "    private Obj a;\r\n" + "    \r\n"
				+ "    public void doSomething(){\r\n" + "        a.doSomethingElse();\r\n" + "    }\r\n" + "}";
		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(code));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();
		Set<String> classFields = new HashSet<String>();
		classFields.add("a");
		PublicMethodsConnectionsMapVisitor visitor = new PublicMethodsConnectionsMapVisitor(classFields);
		ConnectionsMap map = visitor.visit(tree);
		assertTrue(map.getMap().get("doSomething()").contains("a"));
		assertEquals(map.getMap().get("doSomething()").size(), 1);
	}

	@Test
	public void accessAttributeOfClassField() {
		String code = "public class Help{\r\n" + "    private Obj a;\r\n" + "    \r\n"
				+ "    public void doSomething(){\r\n" + "        int b;\r\n" + "        b = a.length;\r\n"
				+ "    }\r\n" + "}";
		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(code));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();
		Set<String> classFields = new HashSet<String>();
		classFields.add("a");
		PublicMethodsConnectionsMapVisitor visitor = new PublicMethodsConnectionsMapVisitor(classFields);
		ConnectionsMap map = visitor.visit(tree);
		assertTrue(map.getMap().get("doSomething()").contains("a"));
		assertEquals(map.getMap().get("doSomething()").size(), 1);
	}

	@Test
	public void passAttributeAsParameterWithThis() {
		String code = "public class Help{\r\n" + "    private int a;\r\n" + "    \r\n"
				+ "    public void doSomething(){\r\n" + "        print(this.a);\r\n" + "    }\r\n" + "}";
		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(code));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();
		Set<String> classFields = new HashSet<String>();
		classFields.add("a");
		PublicMethodsConnectionsMapVisitor visitor = new PublicMethodsConnectionsMapVisitor(classFields);
		ConnectionsMap map = visitor.visit(tree);
		assertTrue(map.getMap().get("doSomething()").contains("a"));
		assertEquals(map.getMap().get("doSomething()").size(), 1);
	}

	@Test
	public void invoqueMethodeOnClassFieldWithThis() {
		String code = "public class Help{\r\n" + "    private Obj a;\r\n" + "    \r\n"
				+ "    public void doSomething(){\r\n" + "        this.a.doSomethingElse();\r\n" + "    }\r\n" + "}";
		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(code));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();
		Set<String> classFields = new HashSet<String>();
		classFields.add("a");
		PublicMethodsConnectionsMapVisitor visitor = new PublicMethodsConnectionsMapVisitor(classFields);
		ConnectionsMap map = visitor.visit(tree);
		assertTrue(map.getMap().get("doSomething()").contains("a"));
		assertEquals(map.getMap().get("doSomething()").size(), 1);
	}

	@Test
	public void accessAttributeOfClassFieldWithThis() {
		String code = "public class Help{\r\n" + "    private Obj a;\r\n" + "    \r\n"
				+ "    public void doSomething(){\r\n" + "        int b;\r\n" + "        b = this.a.length;\r\n"
				+ "    }\r\n" + "}";
		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(code));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();
		Set<String> classFields = new HashSet<String>();
		classFields.add("a");
		PublicMethodsConnectionsMapVisitor visitor = new PublicMethodsConnectionsMapVisitor(classFields);
		ConnectionsMap map = visitor.visit(tree);
		assertTrue(map.getMap().get("doSomething()").contains("a"));
		assertEquals(map.getMap().get("doSomething()").size(), 1);
	}

	@Test
	public void accessAttributeWithSameNameInOtherClass() {
		String code = "public class Help{\r\n" + "    private Obj a;\r\n" + "    \r\n"
				+ "    public void doSomething(){\r\n" + "        Obj other = new Obj();\r\n" + "        int b;\r\n"
				+ "        b = other.a.length;\r\n" + "    }\r\n" + "}";
		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(code));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();
		Set<String> classFields = new HashSet<String>();
		classFields.add("a");
		PublicMethodsConnectionsMapVisitor visitor = new PublicMethodsConnectionsMapVisitor(classFields);
		ConnectionsMap map = visitor.visit(tree);
		assertNull(map.getMap().get("doSomething()"));
	}

	@Test
	public void setAttributeInClassField() {
		String code = "public class Help{\r\n" + "    private Obj a;\r\n" + "    \r\n"
				+ "    public void doSomething(){\r\n" + "        a.attr = 15;\r\n" + "    }\r\n" + "}";
		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(code));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();
		Set<String> classFields = new HashSet<String>();
		classFields.add("a");
		PublicMethodsConnectionsMapVisitor visitor = new PublicMethodsConnectionsMapVisitor(classFields);
		ConnectionsMap map = visitor.visit(tree);
		assertTrue(map.getMap().get("doSomething()").contains("a"));
		assertEquals(map.getMap().get("doSomething()").size(), 1);
	}

	@Test
	public void setAttributeInClassFieldWithThis() {
		String code = "public class Help{\r\n" + "    private Obj a;\r\n" + "    \r\n"
				+ "    public void doSomething(){\r\n" + "        this.a.attr = 15;\r\n" + "    }\r\n" + "}";
		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(code));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();
		Set<String> classFields = new HashSet<String>();
		classFields.add("a");
		PublicMethodsConnectionsMapVisitor visitor = new PublicMethodsConnectionsMapVisitor(classFields);
		ConnectionsMap map = visitor.visit(tree);
		assertTrue(map.getMap().get("doSomething()").contains("a"));
		assertEquals(map.getMap().get("doSomething()").size(), 1);
	}

	@Test
	public void multipleClassFields() {
		String code = "public class Help {\r\n" + "    private Obj a;\r\n" + "    private Obj b;\r\n" + "    \r\n"
				+ "    public void doSomething(){\r\n" + "        a.print(b);\r\n" + "    }\r\n" + "}";
		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(code));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();
		Set<String> classFields = new HashSet<String>();
		classFields.add("a");
		classFields.add("b");
		PublicMethodsConnectionsMapVisitor visitor = new PublicMethodsConnectionsMapVisitor(classFields);
		ConnectionsMap map = visitor.visit(tree);
		assertTrue(map.getMap().get("doSomething()").contains("a"));
		assertTrue(map.getMap().get("doSomething()").contains("b"));
		assertEquals(map.getMap().get("doSomething()").size(), 2);
	}

	@Test
	public void accessClassFieldsFromMultipleMethods() {
		String code = "public class Help{\r\n" + "    private Obj a;\r\n" + "    private Obj b;\r\n" + "    \r\n"
				+ "    public void doSomething(){\r\n" + "        print(a);\r\n" + "    }\r\n" + "    \r\n"
				+ "    public void doSomethingElse(){\r\n" + "        print(b);\r\n" + "    }\r\n" + "}";
		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(code));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();
		Set<String> classFields = new HashSet<String>();
		classFields.add("a");
		classFields.add("b");
		PublicMethodsConnectionsMapVisitor visitor = new PublicMethodsConnectionsMapVisitor(classFields);
		ConnectionsMap map = visitor.visit(tree);
		assertTrue(map.getMap().get("doSomething()").contains("a"));
		assertTrue(map.getMap().get("doSomethingElse()").contains("b"));
		assertEquals(map.getMap().get("doSomething()").size(), 1);
		assertEquals(map.getMap().get("doSomethingElse()").size(), 1);
	}

	@Test
	public void accessClassFieldsFromMultipleMethodsWithPrivate() {
		String code = "public class Help{\r\n" + "    private Obj a;\r\n" + "    private Obj b;\r\n" + "    \r\n"
				+ "    public void doSomething(){\r\n" + "        print(a);\r\n" + "    }\r\n" + "    \r\n"
				+ "    private void doSomethingElse(){\r\n" + "        print(b);\r\n" + "    }\r\n" + "}";
		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(code));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();
		Set<String> classFields = new HashSet<String>();
		classFields.add("a");
		classFields.add("b");
		PublicMethodsConnectionsMapVisitor visitor = new PublicMethodsConnectionsMapVisitor(classFields);
		ConnectionsMap map = visitor.visit(tree);
		assertTrue(map.getMap().get("doSomething()").contains("a"));
		assertEquals(map.getMap().get("doSomething()").size(), 1);
		assertNull(map.getMap().get("doSomethingElse()"));
	}

	@Test
	public void countExpressionInEmbeddedPrimary() {
		String code = "public class Help{\r\n" + "	private Obj className;\r\n" + "	private Obj variableNames;\r\n"
				+ "	public List<VariableDeclaration> toList(){\r\n"
				+ "		List<VariableDeclaration> declarations = new LinkedList<VariableDeclaration>();\r\n"
				+ "		for(String variableName : this.variableNames) {\r\n"
				+ "			VariableDeclaration v = new VariableDeclaration(this.className, variableName);\r\n"
				+ "			declarations.add(v);\r\n" + "		}\r\n" + "		return declarations;\r\n" + "	}}";

		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(code));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();
		Set<String> classFields = new HashSet<String>();
		classFields.add("className");
		classFields.add("variableNames");
		PublicMethodsConnectionsMapVisitor visitor = new PublicMethodsConnectionsMapVisitor(classFields);
		ConnectionsMap map = visitor.visit(tree);
		assertTrue(map.getMap().get("toList()").contains("className"));
		assertTrue(map.getMap().get("toList()").contains("variableNames"));
		assertEquals(map.getMap().get("toList()").size(), 2);
	}
}
