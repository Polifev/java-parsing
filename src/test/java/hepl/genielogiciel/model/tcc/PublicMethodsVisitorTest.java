package hepl.genielogiciel.model.tcc;

import static org.junit.Assert.assertEquals;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.Test;

import hepl.genielogiciel.Java8Lexer;
import hepl.genielogiciel.Java8Parser;

public class PublicMethodsVisitorTest {

	@Test
	public void countPublicMethodNumber() {
		String testClass = "public class GreatClass {\r\n" + "	public GreatClass() {\r\n"
				+ "		MyOtherClass obj1 = new MyOtherClass();\r\n" + "		obj1.prop1 = 12;\r\n" + "	}\r\n"
				+ "	\r\n" + "	public void doSomething(){\r\n" + "	\r\n" + "	}\r\n" + "}";

		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(testClass));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();

		PublicMethodsCountVisitor visitor = new PublicMethodsCountVisitor();
		assertEquals(1, visitor.visit(tree).intValue());
	}

	@Test
	public void countPublicAProtectedMethod() {
		String testClass = "public class GreatClass {\r\n" + "	public GreatClass() {\r\n"
				+ "		MyOtherClass obj1 = new MyOtherClass();\r\n" + "		obj1.prop1 = 12;\r\n" + "	}\r\n"
				+ "	\r\n" + "	public void doSomething(){\r\n" + "	\r\n" + "	}\r\n" + "	\r\n"
				+ "	public void doAnOtherThing(){\r\n" + "	\r\n" + "	}\r\n" + "	void doSomething(){\r\n"
				+ "	\r\n" + "	}\r\n" + "}";

		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(testClass));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();

		PublicMethodsCountVisitor visitor = new PublicMethodsCountVisitor();
		assertEquals(2, visitor.visit(tree).intValue());
	}

	@Test
	public void doesntCountPrivateMethod() {
		String testClass = "public class GreatClass {\r\n" + "	public GreatClass() {\r\n"
				+ "		MyOtherClass obj1 = new MyOtherClass();\r\n" + "		obj1.prop1 = 12;\r\n" + "	}\r\n"
				+ "	\r\n" + "	public void doSomething(){\r\n" + "	\r\n" + "	}\r\n" + "	\r\n"
				+ "	private void doAnOtherThing(){\r\n" + "	\r\n" + "	}\r\n" + "}";

		Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(testClass));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();

		PublicMethodsCountVisitor visitor = new PublicMethodsCountVisitor();
		assertEquals(1, visitor.visit(tree).intValue());
	}

}
