package hepl.genielogiciel.model;

public class GodClassMetric extends AbstractMetric<Boolean> {
	public static final String NAME = "God class";

	public GodClassMetric(Boolean value) {
		super(value);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public boolean isOk(Object threshold) {
		return value() == (Boolean) threshold;
	}

}
