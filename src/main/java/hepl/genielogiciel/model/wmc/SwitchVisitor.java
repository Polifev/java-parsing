package hepl.genielogiciel.model.wmc;

import hepl.genielogiciel.Java8BaseVisitor;
import hepl.genielogiciel.Java8Parser.SwitchBlockStatementGroupContext;

public class SwitchVisitor extends Java8BaseVisitor<Integer> {

	@Override
	protected Integer defaultResult() {
		return 0;
	}

	@Override
	protected Integer aggregateResult(Integer aggregate, Integer nextResult) {
		return aggregate + nextResult;
	}

	@Override
	public Integer visitSwitchBlockStatementGroup(SwitchBlockStatementGroupContext ctx) {
		return new BranchVisitor().visit(ctx);
	}
}
