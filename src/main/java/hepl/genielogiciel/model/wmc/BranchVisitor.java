package hepl.genielogiciel.model.wmc;

import hepl.genielogiciel.Java8BaseVisitor;
import hepl.genielogiciel.Java8Parser.DoStatementContext;
import hepl.genielogiciel.Java8Parser.ForStatementContext;
import hepl.genielogiciel.Java8Parser.IfThenElseStatementContext;
import hepl.genielogiciel.Java8Parser.IfThenStatementContext;
import hepl.genielogiciel.Java8Parser.SwitchStatementContext;
import hepl.genielogiciel.Java8Parser.WhileStatementContext;

public class BranchVisitor extends Java8BaseVisitor<Integer> {

	@Override
	protected Integer defaultResult() {
		return 1;
	}

	@Override
	protected Integer aggregateResult(Integer aggregate, Integer nextResult) {
		return aggregate * nextResult;
	}

	@Override
	public Integer visitIfThenStatement(IfThenStatementContext ctx) {
		return 1 + visitChildren(ctx);
	}

	@Override
	public Integer visitIfThenElseStatement(IfThenElseStatementContext ctx) {
		return 1 + visitChildren(ctx);
	}

	@Override
	public Integer visitWhileStatement(WhileStatementContext ctx) {
		return 1 + visitChildren(ctx);
	}

	@Override
	public Integer visitForStatement(ForStatementContext ctx) {
		return 1 + visitChildren(ctx);
	}

	@Override
	public Integer visitDoStatement(DoStatementContext ctx) {
		return 1 + visitChildren(ctx);
	}

	@Override
	public Integer visitSwitchStatement(SwitchStatementContext ctx) {
		return new SwitchVisitor().visit(ctx);
	}
}
