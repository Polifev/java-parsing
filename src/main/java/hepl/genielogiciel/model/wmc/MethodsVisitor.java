package hepl.genielogiciel.model.wmc;

import hepl.genielogiciel.Java8BaseVisitor;
import hepl.genielogiciel.Java8Parser.ConstructorDeclarationContext;
import hepl.genielogiciel.Java8Parser.MethodDeclarationContext;

public class MethodsVisitor extends Java8BaseVisitor<Integer> {

	@Override
	protected Integer defaultResult() {
		return 0;
	}

	@Override
	protected Integer aggregateResult(Integer aggregate, Integer nextResult) {
		return aggregate + nextResult;
	}

	@Override
	public Integer visitMethodDeclaration(MethodDeclarationContext ctx) {
		return new BranchVisitor().visit(ctx);
	}

	@Override
	public Integer visitConstructorDeclaration(ConstructorDeclarationContext ctx) {
		return new BranchVisitor().visit(ctx);
	}
}
