package hepl.genielogiciel.model.wmc;

import org.antlr.v4.runtime.tree.ParseTree;
import hepl.genielogiciel.Java8Visitor;
import hepl.genielogiciel.model.AbstractMetric;
import hepl.genielogiciel.model.IMetricsCalculator;

public class WeightedMethodCountMetricsCalculator implements IMetricsCalculator<Integer> {
	@Override
	public AbstractMetric<Integer> computeMetric(ParseTree tree) {
		Java8Visitor<Integer> visitor = new MethodsVisitor();
		int metric = visitor.visit(tree);
		return new WmcMetric(metric);
	}
}
