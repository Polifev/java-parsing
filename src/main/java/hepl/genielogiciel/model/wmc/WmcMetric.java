package hepl.genielogiciel.model.wmc;

import hepl.genielogiciel.model.AbstractMetric;

public class WmcMetric extends AbstractMetric<Integer> {
	public static final String NAME = "Weighted methods count";

	public WmcMetric(int value) {
		super(value);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public boolean isOk(Object threshold) {
		return value() < (Integer) threshold;
	}
}
