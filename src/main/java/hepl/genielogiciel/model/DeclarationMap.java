package hepl.genielogiciel.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A class representing all the variable declarations in a Java class. It
 * includes:
 * <ul>
 * <li>Attributes of the class</li>
 * <li>Local variables in the methods of the class</li>
 * <li>Method parameters</li>
 * </ul>
 * 
 * Following the context (method declarator | global context), an instance of
 * this class will give you the type of the given variable.
 * 
 * @author Pol
 *
 */
public class DeclarationMap implements IMergeable<DeclarationMap> {
	public static final String GLOBAL_CONTEXT = "this";

	private Map<String, Map<String, String>> data = new HashMap<String, Map<String, String>>();

	/**
	 * Add a variable declaration.
	 * 
	 * @param context     the method declarator for a local variable, GLOBAL_CONTEXT
	 *                    for a class attribute.
	 * @param declaration A <b>VariableDeclaration</b> instance representing the
	 *                    variable to store.
	 */
	public void addVariable(String context, VariableDeclaration declaration) {
		context = context.replaceAll(" ", "");
		if (!data.containsKey(context)) {
			data.put(context, new HashMap<String, String>());
		}
		data.get(context).put(declaration.getVariableName(), declaration.getClassName());
	}

	/**
	 * Add a variable declaration.
	 * 
	 * @param context      Either:
	 *                     <ul>
	 *                     <li>the method declarator, for a local variable</li>
	 *                     <li>GLOBAL_CONTEXT, for an object attribute</li>
	 * @param declarations A list of <b>VariableDeclaration</b> instance
	 *                     representing the variables to store.
	 */
	public void addVariables(String context, List<VariableDeclaration> declarations) {
		for (VariableDeclaration declaration : declarations) {
			addVariable(context, declaration);
		}
	}

	/**
	 * Gets the type of a variable, given its id and the context in which the
	 * variable appears.
	 * 
	 * @param context      the method declarator for a local variable,
	 *                     GLOBAL_CONTEXT for a class attribute.
	 * @param variableName the variable identifier
	 * @return the type of the variable
	 */
	public String getClassName(String context, String variableName) {
		context = context.replaceAll(" ", "");
		if (data.containsKey(context) && data.get(context).containsKey(variableName)) {
			return data.get(context).get(variableName);
		} else if (data.containsKey(GLOBAL_CONTEXT)) {
			return data.get(GLOBAL_CONTEXT).get(variableName);
		} else {
			throw new IllegalArgumentException("Unknown context or variable");
		}
	}

	public Map<String, String> getClassFields() {
		if (data.containsKey(GLOBAL_CONTEXT)) {
			return data.get(GLOBAL_CONTEXT);
		} else {
			return new HashMap<String, String>();
		}
	}

	@Override
	public DeclarationMap merge(DeclarationMap other) {
		if (other == null)
			return this;

		for (String context : other.data.keySet()) {
			Map<String, String> variables = other.data.get(context);
			for (String variableName : variables.keySet()) {
				VariableDeclaration declaration = new VariableDeclaration(variables.get(variableName), variableName);
				this.addVariable(context, declaration);
			}
		}
		return this;
	}
}
