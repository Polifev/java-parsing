package hepl.genielogiciel.model;

import java.util.Map;

public class GodClassDetector {
	private Map<String, Object> thresholds;

	public GodClassDetector(Map<String, Object> thresholds) {
		this.thresholds = thresholds;
	}

	public void addGodClassMetric(MetricsCollection metrics) {
		for (String file : metrics.scannedFiles()) {
			Map<String, AbstractMetric<?>> metricsForFile = metrics.metricsForFile(file);
			boolean isGodClass = false;
			for (String metric : thresholds.keySet()) {
				if (metricsForFile.containsKey(metric)) {
					Object threshold = thresholds.get(metric);
					IMetric<?> m = metricsForFile.get(metric);
					isGodClass |= (!m.isOk(threshold));
				}
			}
			GodClassMetric resultMetric = new GodClassMetric(isGodClass);
			metricsForFile.put(resultMetric.getName(), resultMetric);
		}
	}
}
