package hepl.genielogiciel.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import hepl.genielogiciel.Java8Lexer;
import hepl.genielogiciel.Java8Parser;

public class ProjectAnalyzer {
	private Set<IMetricsCalculator<?>> calculators = new HashSet<>();

	public ProjectAnalyzer(Set<IMetricsCalculator<?>> calculators) {
		this.calculators = calculators;
	}

	public MetricsCollection analyze(JavaProjectReader projectReader) throws FileNotFoundException, IOException {
		MetricsCollection metricsCollection = new MetricsCollection();
		for (File f : projectReader.getSourceFiles()) {
			try (FileInputStream reader = new FileInputStream(f)) {
				String source = new String(reader.readAllBytes());
				Map<String, AbstractMetric<?>> metrics = metricsCollection.metricsForFile(f.getAbsolutePath());
				for (IMetricsCalculator<?> calculator : calculators) {
					Java8Lexer java8Lexer = new Java8Lexer(CharStreams.fromString(source));
					CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
					Java8Parser parser = new Java8Parser(tokens);
					ParseTree tree = parser.compilationUnit();
					AbstractMetric<?> metric = calculator.computeMetric(tree);
					metrics.put(metric.getName(), metric);
				}
			}
		}
		return metricsCollection;
	}
}
