package hepl.genielogiciel.model;

public abstract class AbstractMetric<T> implements IMetric<T> {
	private T value;

	public AbstractMetric(T value) {
		this.value = value;
	}

	@Override
	public final T value() {
		return this.value;
	}

	@Override
	public String valueAsString() {
		return String.valueOf(value);
	}
}
