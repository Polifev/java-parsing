package hepl.genielogiciel.model;

import org.antlr.v4.runtime.tree.ParseTree;

public interface IMetricsCalculator<T> {
	AbstractMetric<T> computeMetric(ParseTree tree);
}
