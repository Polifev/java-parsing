package hepl.genielogiciel.model;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class GroupedVariableDeclarations implements IMergeable<GroupedVariableDeclarations> {
	private String className;
	private Set<String> variableNames = new HashSet<String>();

	public void setClassName(String className) {
		this.className = className;
	}

	public void addVariableName(String variableName) {
		this.variableNames.add(variableName);
	}

	public List<VariableDeclaration> toList() {
		List<VariableDeclaration> declarations = new LinkedList<VariableDeclaration>();
		for (String variableName : this.variableNames) {
			VariableDeclaration v = new VariableDeclaration(this.className, variableName);
			declarations.add(v);
		}
		return declarations;
	}

	@Override
	public GroupedVariableDeclarations merge(GroupedVariableDeclarations other) {
		if (other == null) {
			return this;
		}
		if (this.className == null) {
			this.className = other.className;
		}
		this.variableNames.addAll(other.variableNames);
		return this;
	}
}
