package hepl.genielogiciel.model.atfd;

import hepl.genielogiciel.Java8BaseVisitor;
import hepl.genielogiciel.Java8Parser.UnannTypeContext;
import hepl.genielogiciel.Java8Parser.VariableDeclaratorIdContext;
import hepl.genielogiciel.model.GroupedVariableDeclarations;

public class GroupedDeclarationVisitor extends Java8BaseVisitor<GroupedVariableDeclarations> {

	@Override
	protected GroupedVariableDeclarations defaultResult() {
		return null;
	}

	@Override
	protected GroupedVariableDeclarations aggregateResult(GroupedVariableDeclarations aggregate,
			GroupedVariableDeclarations nextResult) {
		if (aggregate == null) {
			return nextResult;
		}
		return aggregate.merge(nextResult);
	}

	@Override
	public GroupedVariableDeclarations visitUnannType(UnannTypeContext ctx) {
		GroupedVariableDeclarations result = new GroupedVariableDeclarations();
		result.setClassName(ctx.getText());
		return result;
	}

	@Override
	public GroupedVariableDeclarations visitVariableDeclaratorId(VariableDeclaratorIdContext ctx) {
		GroupedVariableDeclarations result = new GroupedVariableDeclarations();
		result.addVariableName(ctx.getText());
		return result;
	}
}
