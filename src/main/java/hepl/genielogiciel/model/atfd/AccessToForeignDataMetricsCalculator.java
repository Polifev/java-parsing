package hepl.genielogiciel.model.atfd;

import org.antlr.v4.runtime.tree.ParseTree;

import hepl.genielogiciel.model.AbstractMetric;
import hepl.genielogiciel.model.AccessMap;
import hepl.genielogiciel.model.DeclarationMap;
import hepl.genielogiciel.model.IMetricsCalculator;

public class AccessToForeignDataMetricsCalculator implements IMetricsCalculator<Integer> {
	@Override
	public AbstractMetric<Integer> computeMetric(ParseTree tree) {
		// Visit tree
		DeclarationVisitor declarationVisitor = new DeclarationVisitor();
		DeclarationMap declarationMap = declarationVisitor.visit(tree);
		ForeignAccessVisitor foreignAccessVisitor = new ForeignAccessVisitor(declarationMap);
		AccessMap accessMap = foreignAccessVisitor.visit(tree);
		int metric = accessMap == null ? 0 : accessMap.computeResultingCount();

		return new AtfdMetric(metric);
	}
}
