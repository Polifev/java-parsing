package hepl.genielogiciel.model.atfd;

import hepl.genielogiciel.Java8BaseVisitor;
import hepl.genielogiciel.Java8Parser.EnhancedForStatementContext;
import hepl.genielogiciel.Java8Parser.FormalParameterContext;
import hepl.genielogiciel.Java8Parser.LocalVariableDeclarationContext;
import hepl.genielogiciel.model.DeclarationMap;
import hepl.genielogiciel.model.GroupedVariableDeclarations;

public class LocalDeclarationsVisitor extends Java8BaseVisitor<DeclarationMap> {

	private String context;

	public LocalDeclarationsVisitor(String context) {
		super();
		this.context = context;
	}

	@Override
	protected DeclarationMap defaultResult() {
		return null;
	}

	@Override
	protected DeclarationMap aggregateResult(DeclarationMap aggregate, DeclarationMap nextResult) {
		if (aggregate == null) {
			return nextResult;
		}
		return aggregate.merge(nextResult);
	}

	@Override
	public DeclarationMap visitLocalVariableDeclaration(LocalVariableDeclarationContext ctx) {
		DeclarationMap result = new DeclarationMap();
		Java8BaseVisitor<GroupedVariableDeclarations> visitor = new GroupedDeclarationVisitor();
		result.addVariables(this.context, visitor.visit(ctx).toList());
		return result;
	}

	@Override
	public DeclarationMap visitFormalParameter(FormalParameterContext ctx) {
		DeclarationMap result = new DeclarationMap();
		Java8BaseVisitor<GroupedVariableDeclarations> visitor = new GroupedDeclarationVisitor();
		result.addVariables(this.context, visitor.visit(ctx).toList());
		return result;
	}

	@Override
	public DeclarationMap visitEnhancedForStatement(EnhancedForStatementContext ctx) {
		DeclarationMap result = super.visitEnhancedForStatement(ctx);
		if (result == null)
			result = new DeclarationMap();
		Java8BaseVisitor<GroupedVariableDeclarations> visitor = new GroupedDeclarationVisitor();
		result.addVariables(this.context, visitor.visit(ctx).toList());
		return result;
	}
}
