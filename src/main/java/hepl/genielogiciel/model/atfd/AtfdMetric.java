package hepl.genielogiciel.model.atfd;

import hepl.genielogiciel.model.AbstractMetric;

public class AtfdMetric extends AbstractMetric<Integer> {
	public static final String NAME = "Access to foreign data";

	public AtfdMetric(int value) {
		super(value);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public boolean isOk(Object threshold) {
		return value() < (Integer) threshold;
	}
}
