package hepl.genielogiciel.model.atfd;

import hepl.genielogiciel.Java8BaseVisitor;
import hepl.genielogiciel.Java8Parser.ConstructorDeclarationContext;
import hepl.genielogiciel.Java8Parser.MethodDeclarationContext;
import hepl.genielogiciel.Java8Parser.NormalClassDeclarationContext;
import hepl.genielogiciel.Java8Visitor;
import hepl.genielogiciel.model.AccessMap;
import hepl.genielogiciel.model.DeclarationMap;

public class ForeignAccessVisitor extends Java8BaseVisitor<AccessMap> {
	private DeclarationMap registeredVariables;

	public ForeignAccessVisitor(DeclarationMap registeredVariables) {
		super();
		this.registeredVariables = registeredVariables;
	}

	@Override
	protected AccessMap defaultResult() {
		return null;
	}

	@Override
	protected AccessMap aggregateResult(AccessMap aggregate, AccessMap nextResult) {
		if (aggregate == null) {
			return nextResult;
		}
		return aggregate.merge(nextResult);
	}

	@Override
	public AccessMap visitMethodDeclaration(MethodDeclarationContext ctx) {
		Java8Visitor<String> nameVisitor = new MethodNameVisitor();
		String context = nameVisitor.visit(ctx);
		Java8Visitor<AccessMap> accessVisitor = new LocalAccessVisitor(this.registeredVariables, context);
		return accessVisitor.visit(ctx);
	}

	@Override
	public AccessMap visitConstructorDeclaration(ConstructorDeclarationContext ctx) {
		Java8Visitor<String> nameVisitor = new ConstructorNameVisitor();
		String context = nameVisitor.visit(ctx);
		Java8Visitor<AccessMap> accessVisitor = new LocalAccessVisitor(this.registeredVariables, context);
		return accessVisitor.visit(ctx);
	}

	@Override
	public AccessMap visitNormalClassDeclaration(NormalClassDeclarationContext ctx) {
		AccessMap result = super.visitNormalClassDeclaration(ctx);
		if (result == null) {
			result = new AccessMap();
		}

		String className = "";
		int i = 0;
		while (!className.equals("class")) {
			className = ctx.getChild(i++).getText();
		}
		className = ctx.getChild(i).getText();
		result.ignoreAccess(className);
		return result;
	}
}
