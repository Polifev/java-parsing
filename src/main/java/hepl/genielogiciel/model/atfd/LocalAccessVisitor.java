package hepl.genielogiciel.model.atfd;

import org.antlr.v4.runtime.ParserRuleContext;

import hepl.genielogiciel.Java8BaseVisitor;
import hepl.genielogiciel.Java8Parser.ExpressionNameContext;
import hepl.genielogiciel.Java8Parser.MethodInvocationContext;
import hepl.genielogiciel.Java8Parser.MethodInvocation_lfno_primaryContext;
import hepl.genielogiciel.model.AccessMap;
import hepl.genielogiciel.model.DeclarationMap;

public class LocalAccessVisitor extends Java8BaseVisitor<AccessMap> {

	private DeclarationMap registeredVariables;
	private String context;

	public LocalAccessVisitor(DeclarationMap registeredVariables, String context) {
		super();
		this.registeredVariables = registeredVariables;
		this.context = context;
	}

	@Override
	protected AccessMap defaultResult() {
		return null;
	}

	@Override
	protected AccessMap aggregateResult(AccessMap aggregate, AccessMap nextResult) {
		if (aggregate == null) {
			return nextResult;
		}
		return aggregate.merge(nextResult);
	}

	@Override
	public AccessMap visitExpressionName(ExpressionNameContext ctx) {
		String txt = ctx.getText();
		AccessMap result = new AccessMap();
		if (txt.indexOf('.') != -1) {
			String variableName = txt.substring(0, txt.indexOf('.'));
			try {
				String className = registeredVariables.getClassName(context, variableName);
				result.registerAccess(className);
			} catch (IllegalArgumentException e) {
				// Skip
			}
		}
		return result;
	}

	@Override
	public AccessMap visitMethodInvocation(MethodInvocationContext ctx) {
		AccessMap result = super.visitMethodInvocation(ctx);
		if (result == null)
			result = new AccessMap();
		detectsGettersSetters(ctx, result);
		return result;
	}

	@Override
	public AccessMap visitMethodInvocation_lfno_primary(MethodInvocation_lfno_primaryContext ctx) {
		AccessMap result = super.visitMethodInvocation_lfno_primary(ctx);
		if (result == null)
			result = new AccessMap();
		detectsGettersSetters(ctx, result);
		return result;
	}

	private void detectsGettersSetters(ParserRuleContext ctx, AccessMap map) {
		String txt = ctx.getText();
		if (txt.contains(".")) {
			String variableName = txt.substring(0, txt.indexOf('.'));
			String methodName = txt.substring(0, txt.indexOf('('));
			methodName = methodName.substring(methodName.lastIndexOf('.') + 1);

			if (!variableName.equals("this") && (methodName.startsWith("get") || methodName.startsWith("set"))
					&& (methodName.length() <= 3 || Character.isUpperCase(methodName.charAt(3)))) {
				try {
					String className = registeredVariables.getClassName(context, variableName);
					map.registerAccess(className);
				} catch (IllegalArgumentException e) {
					// Skip
				}
			}
		}

	}
}
