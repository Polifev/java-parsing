package hepl.genielogiciel.model.atfd;

import hepl.genielogiciel.Java8BaseVisitor;
import hepl.genielogiciel.Java8Parser.ConstructorDeclaratorContext;

public class ConstructorNameVisitor extends Java8BaseVisitor<String> {
	@Override
	protected String defaultResult() {
		return null;
	}

	@Override
	protected String aggregateResult(String aggregate, String nextResult) {
		if (aggregate != null) {
			return aggregate;
		} else {
			return nextResult;
		}
	}

	@Override
	public String visitConstructorDeclarator(ConstructorDeclaratorContext ctx) {
		return ctx.getText();
	}
}
