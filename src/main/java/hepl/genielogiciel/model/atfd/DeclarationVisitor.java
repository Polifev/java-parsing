package hepl.genielogiciel.model.atfd;

import hepl.genielogiciel.Java8BaseVisitor;
import hepl.genielogiciel.Java8Parser.ConstructorDeclarationContext;
import hepl.genielogiciel.Java8Parser.FieldDeclarationContext;
import hepl.genielogiciel.Java8Parser.MethodDeclarationContext;
import hepl.genielogiciel.Java8Visitor;
import hepl.genielogiciel.model.DeclarationMap;
import hepl.genielogiciel.model.GroupedVariableDeclarations;

public class DeclarationVisitor extends Java8BaseVisitor<DeclarationMap> {
	@Override
	protected DeclarationMap defaultResult() {
		return null;
	}

	@Override
	protected DeclarationMap aggregateResult(DeclarationMap aggregate, DeclarationMap nextResult) {
		if (aggregate == null) {
			return nextResult;
		}
		return aggregate.merge(nextResult);
	}

	@Override
	public DeclarationMap visitFieldDeclaration(FieldDeclarationContext ctx) {
		DeclarationMap result = new DeclarationMap();
		Java8Visitor<GroupedVariableDeclarations> visitor = new GroupedDeclarationVisitor();
		result.addVariables(DeclarationMap.GLOBAL_CONTEXT, visitor.visit(ctx).toList());
		return result;
	}

	@Override
	public DeclarationMap visitMethodDeclaration(MethodDeclarationContext ctx) {
		Java8Visitor<String> methodDeclarationVisitor = new MethodNameVisitor();
		String context = methodDeclarationVisitor.visit(ctx);
		Java8Visitor<DeclarationMap> localDeclarationsVisitor = new LocalDeclarationsVisitor(context);
		return localDeclarationsVisitor.visit(ctx);
	}

	@Override
	public DeclarationMap visitConstructorDeclaration(ConstructorDeclarationContext ctx) {
		Java8Visitor<String> constructorNameVisitor = new ConstructorNameVisitor();
		String context = constructorNameVisitor.visit(ctx);
		Java8Visitor<DeclarationMap> localDeclarationsVisitor = new LocalDeclarationsVisitor(context);
		return localDeclarationsVisitor.visit(ctx);
	}
}
