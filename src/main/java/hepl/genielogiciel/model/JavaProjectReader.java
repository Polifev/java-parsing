package hepl.genielogiciel.model;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class JavaProjectReader {
	private static final String NOT_FOUND = "File or directory not found";
	private static final String EXTENSION = ".java";
	private String rootPath;
	private File root;

	public JavaProjectReader(String rootPath) {
		this.rootPath = rootPath;
		Path p = Path.of(this.rootPath);
		root = p.toFile();
	}

	public File root() {
		return this.root;
	}

	public Iterable<File> getSourceFiles() throws IOException {
		List<File> sourceFiles = new ArrayList<File>();

		if (root.exists()) {
			if (root.isDirectory()) {
				for (File sub : getFilesRec(root)) {
					String name = sub.getName();
					if (name.endsWith(EXTENSION)) {
						sourceFiles.add(sub);
					}
				}
			} else if (root.getName().endsWith(EXTENSION)) {
				sourceFiles.add(root);
			}
			return sourceFiles;
		} else {
			throw new IOException(NOT_FOUND);
		}
	}

	private static List<File> getFilesRec(File dir) {
		List<File> result = new LinkedList<File>();
		String[] content = dir.list();
		for (int i = 0; i < content.length; i++) {
			File f = Path.of(dir.getAbsolutePath(), content[i]).toFile();
			if (f.isDirectory()) {
				result.addAll(getFilesRec(f));
			} else {
				result.add(f);
			}
		}
		return result;
	}
}
