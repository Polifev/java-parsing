package hepl.genielogiciel.model;

import java.util.HashSet;
import java.util.Set;

public class AccessMap implements IMergeable<AccessMap> {
	private Set<String> registeredAccess = new HashSet<String>();
	private Set<String> ignoredAccess = new HashSet<String>();

	public void registerAccess(String className) {
		registeredAccess.add(className);
	}

	public void ignoreAccess(String className) {
		ignoredAccess.add(className);
	}

	public int computeResultingCount() {
		Set<String> resultingAccess = new HashSet<String>();
		resultingAccess.addAll(registeredAccess);
		resultingAccess.removeAll(ignoredAccess);
		return resultingAccess.size();
	}

	@Override
	public AccessMap merge(AccessMap other) {
		if (other == null) {
			return this;
		}
		this.registeredAccess.addAll(other.registeredAccess);
		this.ignoredAccess.addAll(other.ignoredAccess);
		return this;
	}
}
