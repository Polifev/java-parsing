package hepl.genielogiciel.model.builder;

import java.util.HashMap;
import java.util.Map;

import hepl.genielogiciel.model.GodClassDetector;

public class GodClassDetectorBuilder {
	private Map<String, Object> thresholds = new HashMap<String, Object>();

	public GodClassDetectorBuilder addThreshold(String metricName, Object threshold) {
		thresholds.put(metricName, threshold);
		return this;
	}

	public GodClassDetector build() {
		return new GodClassDetector(thresholds);
	}
}
