package hepl.genielogiciel.model.builder;

import java.util.HashSet;
import java.util.Set;

import hepl.genielogiciel.model.IMetricsCalculator;
import hepl.genielogiciel.model.ProjectAnalyzer;

public class ProjectAnalyzerBuilder {
	private Set<IMetricsCalculator<?>> calculators = new HashSet<>();

	public ProjectAnalyzerBuilder addMetricCalculator(IMetricsCalculator<?> calculator) {
		calculators.add(calculator);
		return this;
	}

	public ProjectAnalyzer build() {
		return new ProjectAnalyzer(calculators);
	}

}
