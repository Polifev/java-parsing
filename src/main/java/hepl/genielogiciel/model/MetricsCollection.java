package hepl.genielogiciel.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MetricsCollection {
	private Map<String, Map<String, AbstractMetric<?>>> metrics = new HashMap<>();

	public Map<String, AbstractMetric<?>> metricsForFile(String file) {
		if (!metrics.containsKey(file)) {
			metrics.put(file, new HashMap<String, AbstractMetric<?>>());
		}
		return metrics.get(file);
	}

	public Set<String> scannedFiles() {
		return metrics.keySet();
	}
}
