package hepl.genielogiciel.model;

/**
 * A class representing the declaration of a variable, specifying the variable
 * identifier and its type name.
 * 
 * @author Pol
 *
 */
public class VariableDeclaration {
	private String className, variableName;

	public VariableDeclaration() {
		this(null, null);
	}

	public VariableDeclaration(String className, String variableName) {
		this.className = className;
		this.variableName = variableName;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public void setVariableName(String variableName) {
		this.variableName = variableName;
	}

	public String getClassName() {
		return className;
	}

	public String getVariableName() {
		return variableName;
	}
}
