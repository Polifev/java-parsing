package hepl.genielogiciel.model.tcc;

import hepl.genielogiciel.Java8BaseVisitor;
import hepl.genielogiciel.Java8Parser.MethodDeclarationContext;

public class PublicMethodsCountVisitor extends Java8BaseVisitor<Integer> {

	@Override
	protected Integer defaultResult() {
		return 0;
	}

	@Override
	protected Integer aggregateResult(Integer aggregate, Integer nextResult) {
		return aggregate + nextResult;
	}

	public Integer visitMethodDeclaration(MethodDeclarationContext ctx) {
		return ctx.getText().startsWith("public") || ctx.getText().startsWith("@Overridepublic") ? 1 : 0;
	}

}
