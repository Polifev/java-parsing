package hepl.genielogiciel.model.tcc;

public class Couple {

	private String m1, m2;

	public Couple(String m1, String m2) {
		this.m1 = m1;
		this.m2 = m2;
	}

	@Override
	public boolean equals(Object obj) {
		Couple other = (Couple) obj;
		// 1 = 1 && 2 = 2 || 1 = 2 && 2 == 1
		return (m1.equals(other.m1) && m2.equals(other.m2)) || (m1.equals(other.m2) && m2.equals(other.m1));
	}

	@Override
	public int hashCode() {
		return m1.hashCode() + m2.hashCode();
	}

	@Override
	public String toString() {
		return String.format("%s~%s", m1, m2);
	}
}
