package hepl.genielogiciel.model.tcc;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.antlr.v4.runtime.tree.ParseTree;

import hepl.genielogiciel.model.AbstractMetric;
import hepl.genielogiciel.model.IMetricsCalculator;

public class TightClassCohesionMetricsCalculator implements IMetricsCalculator<Double> {
	@Override
	public AbstractMetric<Double> computeMetric(ParseTree tree) {
		double maxConnections = getMaxConnections(tree);
		if (maxConnections == 0)
			return new TccMetric(1.0); // Si pas de méthodes publiques, cohesion de 1
		Set<String> classFields = getClassFields(tree);
		if (classFields.size() == 0)
			return new TccMetric(0.0); // Si pas de champs, cohesion de 0

		Map<String, Set<String>> map = getClassFieldsForMethod(tree, classFields);
		Map<String, Set<String>> reverseMap = mapToReverseMap(map);

		// 3. établir une liste de connections // On fait un set pour éviter de
		// compter les doublons // La classe couple permet de lier deux méthodes //
		// L'ordre n'a pas d'importance pour redéfinir equals et hashCode
		// set<Couple> totalCon = []; foreach method in map.keys{ foreach variable in
		// map[method]{ totalCon.add( new Couple(method, reverseMap[variable]) ) } }
		// total = totalCon.size()

		Set<Couple> couples = new HashSet<Couple>();
		for (String method : map.keySet()) {
			for (String classField : map.get(method)) {
				for (String methodInReverse : reverseMap.get(classField)) {
					if (!method.equals(methodInReverse)) {
						couples.add(new Couple(method, methodInReverse));
					}
				}
			}
		}

		double total = couples.size();
		return new TccMetric(total / maxConnections);
	}

	private int getMaxConnections(ParseTree tree) {
		// 0. Calcul de maxConnections n = publicMethods.length
		// maxConnections = (n * (n - 1)) / 2)
		int n = new PublicMethodsCountVisitor().visit(tree);
		return (n * (n - 1)) / 2;
	}

	private Set<String> getClassFields(ParseTree tree) {
		// 1. établir une liste des attributs de la classe listeVariableMembres = [a, b,
		// c]
		Map<String, String> classFields = new ClassFieldVisitor().visit(tree).getClassFields();
		return classFields.keySet();
	}

	private Map<String, Set<String>> getClassFieldsForMethod(ParseTree tree, Set<String> classFields) {
		// 2. établir une relation entre les méthodes et l'accès aux attributs
		// map = [ f1: [a], f2: [b,c], f3: [a,c], f4: [c] ]
		//
		// reverseMap = [ a: [f1, f3], b: [f2], c: [f2, f3, f4] ]
		return new PublicMethodsConnectionsMapVisitor(classFields).visit(tree).getMap();
	}

	private Map<String, Set<String>> mapToReverseMap(Map<String, Set<String>> map) {
		Map<String, Set<String>> reverse = new HashMap<String, Set<String>>();
		for (String method : map.keySet()) {
			for (String classField : map.get(method)) {
				if (!reverse.containsKey(classField)) {
					reverse.put(classField, new HashSet<String>());
				}
				reverse.get(classField).add(method);
			}
		}
		return reverse;
	}
}