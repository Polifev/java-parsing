package hepl.genielogiciel.model.tcc;

import hepl.genielogiciel.Java8Visitor;
import hepl.genielogiciel.Java8BaseVisitor;
import hepl.genielogiciel.Java8Parser.FieldDeclarationContext;
import hepl.genielogiciel.model.DeclarationMap;
import hepl.genielogiciel.model.GroupedVariableDeclarations;
import hepl.genielogiciel.model.atfd.GroupedDeclarationVisitor;

public class ClassFieldVisitor extends Java8BaseVisitor<DeclarationMap> {

	@Override
	protected DeclarationMap defaultResult() {
		return new DeclarationMap();
	}

	@Override
	protected DeclarationMap aggregateResult(DeclarationMap aggregate, DeclarationMap nextResult) {
		return aggregate.merge(nextResult);
	}

	@Override
	public DeclarationMap visitFieldDeclaration(FieldDeclarationContext ctx) {
		DeclarationMap result = new DeclarationMap();
		Java8Visitor<GroupedVariableDeclarations> visitor = new GroupedDeclarationVisitor();
		result.addVariables(DeclarationMap.GLOBAL_CONTEXT, visitor.visit(ctx).toList());
		return result;
	}
}
