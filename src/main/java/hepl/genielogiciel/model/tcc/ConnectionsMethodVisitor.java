package hepl.genielogiciel.model.tcc;

import java.util.Set;

import hepl.genielogiciel.Java8BaseVisitor;
import hepl.genielogiciel.Java8Parser.ExpressionNameContext;
import hepl.genielogiciel.Java8Parser.MethodInvocationContext;
import hepl.genielogiciel.Java8Parser.PrimaryContext;

public class ConnectionsMethodVisitor extends Java8BaseVisitor<ConnectionsMap> {

	private String methodName;
	private Set<String> classFields;

	public ConnectionsMethodVisitor(Set<String> classFields, String methodName) {
		this.classFields = classFields;
		this.methodName = methodName;
	}

	@Override
	protected ConnectionsMap defaultResult() {
		return new ConnectionsMap();
	}

	@Override
	public ConnectionsMap visitExpressionName(ExpressionNameContext ctx) {
		String txt = ctx.getText();
		ConnectionsMap result = new ConnectionsMap();
		if (txt.indexOf('.') != -1) {
			String[] split = txt.split("\\.");
			String variableName = split[0];
			if (variableName.equals("this") && split.length >= 2) {
				variableName = split[1];
			}
			if (classFields.contains(variableName)) {
				result.addClassFieldForMethod(variableName, methodName);
			}
		} else {
			String variableName = txt;
			if (classFields.contains(variableName)) {
				result.addClassFieldForMethod(variableName, methodName);
			}
		}
		return result;
	}

	@Override
	public ConnectionsMap visitPrimary(PrimaryContext ctx) {
		String txt = ctx.getText();
		ConnectionsMap result = super.visitPrimary(ctx);
		if (txt.indexOf('.') != -1) {
			String[] split = txt.split("\\.");
			String variableName = split[0];
			if (variableName.equals("this") && split.length >= 2) {
				variableName = split[1];
			}
			if (classFields.contains(variableName)) {
				result.addClassFieldForMethod(variableName, methodName);
			}
		} else {
			String variableName = txt;
			if (classFields.contains(variableName)) {
				result.addClassFieldForMethod(variableName, methodName);
			}
		}
		return result;
	}

	@Override
	public ConnectionsMap visitMethodInvocation(MethodInvocationContext ctx) {
		String txt = ctx.getText();
		ConnectionsMap result = super.visitMethodInvocation(ctx);
		if (txt.contains(".")) {
			String[] split = txt.split("\\.");
			String variableName = split[0];
			if (variableName.equals("this") && split.length >= 2) {
				variableName = split[1];
			}
			if (classFields.contains(variableName)) {
				result.addClassFieldForMethod(variableName, methodName);
			}
		}
		return result;
	}

	@Override
	protected ConnectionsMap aggregateResult(ConnectionsMap aggregate, ConnectionsMap nextResult) {
		return aggregate.merge(nextResult);
	}

}
