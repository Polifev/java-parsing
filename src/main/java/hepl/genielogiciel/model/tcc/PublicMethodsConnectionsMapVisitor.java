package hepl.genielogiciel.model.tcc;

import java.util.Set;

import hepl.genielogiciel.Java8BaseVisitor;
import hepl.genielogiciel.Java8Parser.MethodDeclarationContext;
import hepl.genielogiciel.model.atfd.MethodNameVisitor;

public class PublicMethodsConnectionsMapVisitor extends Java8BaseVisitor<ConnectionsMap> {

	private Set<String> classFields;

	public PublicMethodsConnectionsMapVisitor(Set<String> classFields) {
		this.classFields = classFields;
	}

	@Override
	protected ConnectionsMap defaultResult() {
		return new ConnectionsMap();
	}

	@Override
	protected ConnectionsMap aggregateResult(ConnectionsMap aggregate, ConnectionsMap nextResult) {
		return aggregate.merge(nextResult);
	}

	@Override
	public ConnectionsMap visitMethodDeclaration(MethodDeclarationContext ctx) {
		String methodName = new MethodNameVisitor().visit(ctx);
		return ctx.getText().startsWith("public") || ctx.getText().startsWith("@Overridepublic")
				? new ConnectionsMethodVisitor(classFields, methodName).visit(ctx)
				: new ConnectionsMap();
	}

}
