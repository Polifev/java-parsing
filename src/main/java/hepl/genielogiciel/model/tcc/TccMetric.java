package hepl.genielogiciel.model.tcc;

import hepl.genielogiciel.model.AbstractMetric;

public class TccMetric extends AbstractMetric<Double> {
	public static final String NAME = "Tight class cohesion";

	public TccMetric(Double value) {
		super(value);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public boolean isOk(Object threshold) {
		return value() > (Double) threshold;
	}

	@Override
	public String valueAsString() {
		return String.format("%.1f%%", value() * 100);
	}
}