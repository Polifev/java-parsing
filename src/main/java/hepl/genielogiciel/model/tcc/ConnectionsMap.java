package hepl.genielogiciel.model.tcc;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import hepl.genielogiciel.model.IMergeable;

public class ConnectionsMap implements IMergeable<ConnectionsMap> {

	private Map<String, Set<String>> map;

	public ConnectionsMap() {
		map = new HashMap<String, Set<String>>();
	}

	public Map<String, Set<String>> getMap() {
		return map;
	}

	public void addClassFieldForMethod(String classField, String method) {
		if (!map.containsKey(method)) {
			map.put(method, new HashSet<String>());
		}
		map.get(method).add(classField);
	}

	@Override
	public ConnectionsMap merge(ConnectionsMap other) {
		for (String method : other.map.keySet()) {
			Set<String> set = other.map.get(method);
			if (this.map.containsKey(method)) {
				this.map.get(method).addAll(set);
			} else {
				this.map.put(method, set);
			}
		}
		return this;
	}

}
