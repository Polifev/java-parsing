package hepl.genielogiciel.model;

/**
 * This interface represents an object that can be merged with an object of the
 * T class, resulting a T class object.
 * 
 * @author Pol
 * @param <T>The type of object this class can merge with
 */
public interface IMergeable<T> {
	/**
	 * Merge the current instance with the one provided as parameter and return the
	 * result.
	 * 
	 * @param other The object you want to merge with
	 * @return the merge result
	 */
	public T merge(T other);
}
