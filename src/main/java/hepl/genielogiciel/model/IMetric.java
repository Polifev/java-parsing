package hepl.genielogiciel.model;

public interface IMetric<T> {
	String getName();

	boolean isOk(Object threshold);

	T value();

	String valueAsString();
}
