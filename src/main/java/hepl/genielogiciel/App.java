package hepl.genielogiciel;

import java.io.IOException;

import hepl.genielogiciel.model.GodClassDetector;
import hepl.genielogiciel.model.JavaProjectReader;
import hepl.genielogiciel.model.MetricsCollection;
import hepl.genielogiciel.model.ProjectAnalyzer;
import hepl.genielogiciel.model.atfd.AccessToForeignDataMetricsCalculator;
import hepl.genielogiciel.model.atfd.AtfdMetric;
import hepl.genielogiciel.model.builder.GodClassDetectorBuilder;
import hepl.genielogiciel.model.builder.ProjectAnalyzerBuilder;
import hepl.genielogiciel.model.tcc.TccMetric;
import hepl.genielogiciel.model.tcc.TightClassCohesionMetricsCalculator;
import hepl.genielogiciel.model.wmc.WeightedMethodCountMetricsCalculator;
import hepl.genielogiciel.model.wmc.WmcMetric;
import hepl.genielogiciel.output.IOutputProvider;
import hepl.genielogiciel.output.OutputProviderFactory;

public class App {
	public static void main(String[] args) {
		String rootPath = readArg(args, 0, ".");
		String outputFormat = readArg(args, 1, OutputProviderFactory.HUMAN_READABLE);
		int atfdThreshold = Integer.parseInt(readArg(args, 2, "5"));
		int wmcThreshold = Integer.parseInt(readArg(args, 3, "50"));
		double tccThreshold = Double.parseDouble(readArg(args, 4, "0.4"));

		JavaProjectReader projectReader = new JavaProjectReader(rootPath);

		ProjectAnalyzer analyzer = new ProjectAnalyzerBuilder()
				.addMetricCalculator(new AccessToForeignDataMetricsCalculator())
				.addMetricCalculator(new WeightedMethodCountMetricsCalculator())
				.addMetricCalculator(new TightClassCohesionMetricsCalculator()).build();

		GodClassDetector detector = new GodClassDetectorBuilder().addThreshold(AtfdMetric.NAME, atfdThreshold)
				.addThreshold(WmcMetric.NAME, wmcThreshold).addThreshold(TccMetric.NAME, tccThreshold).build();
		try {
			MetricsCollection metricsCollection = analyzer.analyze(projectReader);
			detector.addGodClassMetric(metricsCollection);
			IOutputProvider out = OutputProviderFactory.getOutputProvider(outputFormat);
			out.print(metricsCollection);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	private static String readArg(String[] args, int index, String def) {
		return (args.length > index) ? args[index] : def;
	}
}
