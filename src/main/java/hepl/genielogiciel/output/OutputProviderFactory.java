package hepl.genielogiciel.output;

import com.google.gson.GsonBuilder;

public class OutputProviderFactory {

	public static final String RAW_JSON = "raw-json";
	public static final String PRETTY_JSON = "pretty-json";
	public static final String HUMAN_READABLE = "human-readable";

	public static IOutputProvider getOutputProvider(String providerName) {
		switch (providerName) {
		case RAW_JSON:
			return new GsonOutputProvider(new GsonBuilder().create());
		case PRETTY_JSON:
			return new GsonOutputProvider(new GsonBuilder().setPrettyPrinting().create());
		case HUMAN_READABLE:
			return new HumanReadableOutputProvider();
		default:
			throw new IllegalArgumentException(String.format("Provider not found: \"%s\"", providerName));
		}
	}
}
