package hepl.genielogiciel.output;

import hepl.genielogiciel.model.MetricsCollection;

public interface IOutputProvider {
	public void print(MetricsCollection metricsCollection);
}
