package hepl.genielogiciel.output;

import java.util.Map;

import hepl.genielogiciel.model.AbstractMetric;
import hepl.genielogiciel.model.MetricsCollection;

public class HumanReadableOutputProvider implements IOutputProvider {

	@Override
	public void print(MetricsCollection metricsCollection) {
		for (String s : metricsCollection.scannedFiles()) {
			Map<String, AbstractMetric<?>> map = metricsCollection.metricsForFile(s);
			System.out.println("[SCANNED]: " + s);
			for (String metricName : map.keySet()) {
				String value = map.get(metricName).valueAsString();
				System.out.printf("\t* %-30s%10s\n", metricName, value);
			}
			System.out.println();
		}
	}

}
