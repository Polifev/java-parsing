package hepl.genielogiciel.output;

import com.google.gson.Gson;

import hepl.genielogiciel.model.MetricsCollection;

public class GsonOutputProvider implements IOutputProvider {
	private Gson gson;

	public GsonOutputProvider(Gson gson) {
		this.gson = gson;
	}

	@Override
	public void print(MetricsCollection metricsCollection) {
		System.out.println(gson.toJson(metricsCollection));
	}
}
